package com.practice.blog.config;

public class AppConstants {
	public static final String PAGE_NUMBER = "0";
	public static final String PAGE_SIZE = "5";
	public static final String SORT_BY = "id";
	public static final String SORT_DIRECTION = "asc";
	public static final Integer ROLE_ADMIN = 501;
	public static final Integer ROLE_USER = 502;
}
