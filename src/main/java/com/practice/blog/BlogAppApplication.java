package com.practice.blog;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.practice.blog.config.AppConstants;
import com.practice.blog.entities.Role;
import com.practice.blog.repositories.RoleRepo;

@SpringBootApplication
public class BlogAppApplication implements CommandLineRunner {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleRepo roleRepo;

	public static void main(String[] args) {
		SpringApplication.run(BlogAppApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(this.passwordEncoder.encode("x"));

		try {
			Role roleAdmin = new Role();
			roleAdmin.setId(AppConstants.ROLE_ADMIN);
			roleAdmin.setName("ROLE_ADMIN");

			Role roleUser = new Role();
			roleUser.setId(AppConstants.ROLE_USER);
			roleUser.setName("ROLE_USER");

			List<Role> roles = new ArrayList<>();
			roles.add(roleAdmin);
			roles.add(roleUser);

			List<Role> result = this.roleRepo.saveAll(roles);

			result.forEach(r -> {
				System.out.println(r.getName());
			});
		} catch (Exception e) {
			System.out.println("Exception Occurred !!");
			e.printStackTrace();
		}
	}
}
