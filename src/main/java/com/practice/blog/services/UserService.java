package com.practice.blog.services;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.practice.blog.payloads.UserDto;

public interface UserService {

	// REGISTER
	UserDto registerUser(UserDto userDto);

	// CREATE
	UserDto createUser(UserDto userDto);

	// READ SINGLE
	UserDto getUser(Integer userId);

	// READ ALL
	List<UserDto> getUsers();

	// UPDATE
	UserDto updateUser(UserDto userDto, Integer userId);

	// DELETE
	@PreAuthorize("hasRole('ADMIN')")
	void deleteUser(Integer userId);
}
